import Swiper, { Navigation, Pagination } from 'swiper/core';

Swiper.use([Navigation, Pagination]);

//Инициализируем слайдер в секции About
const swiper = new Swiper('.about__slider_container', {
    slidesPerView: 2,
    spaceBetween: 70,
    direction: 'vertical',
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
});

//Меню аккардеон в секции Services
const servicesBoxTitle = document.querySelectorAll('.services__box_title'),
      servicesBoxContent = document.querySelectorAll('.services__box_content'),
      titleArrow = document.querySelectorAll('.title__arrow'),
      video = document.querySelector('.video'),
      headerVideoPlay = document.querySelector('.header__video_play'),
      videoPlay = document.querySelector('.video__play_btn');

for(let i = 0; i < servicesBoxTitle.length; i++){
    servicesBoxTitle[i].addEventListener('click', () =>{
        servicesBoxContent[i].classList.toggle('services__box_content_visible');
        if(servicesBoxContent[i].classList.contains('services__box_content_visible')){
            servicesBoxContent[i].style.maxHeight = servicesBoxContent[i].scrollHeight + 'px';
        } else {
            servicesBoxContent[i].style.maxHeight = null;
        }
        titleArrow[i].classList.toggle('title__arrow_active');
    })
};

//Воспроизведение видео в хедере
videoPlay.addEventListener('click', ()=> {
    video.play();
    headerVideoPlay.style.opacity = '0';
    headerVideoPlay.style.visibility = 'hidden';
    headerVideoPlay.style.transition = 'all 0.3s ease-in-out';
})

//Мобильное меню
const headerNavMobile = document.querySelector('.header__nav_mobile'),
      headerMenuMobile = document.querySelector('.header__menu_mobile'),
      closeMobile = document.querySelector('.header__nav_mobile_close');

const mobileMenu = () =>{
    headerNavMobile.addEventListener('click', () =>{
        headerMenuMobile.classList.add('header__menu_mobile--open');
        closeMobile.style.display = 'block';
        headerNavMobile.style.backgroundImage = 'none';
    })
    
    closeMobile.addEventListener('click', () =>{
        headerMenuMobile.classList.remove('header__menu_mobile--open');
        headerNavMobile.style.backgroundImage = "url('../img/burger-menu.png')";
        closeMobile.style.display = 'none';
    })

    if(window.innerWidth > 850){
        headerNavMobile.style.backgroundImage = 'none';
    } else {
        headerNavMobile.style.backgroundImage = "url('../img/burger-menu.png')";
    }
}

if(window.innerWidth <= 850){
    mobileMenu();
}

var sliderTeam;

const teamSlider = document.querySelector('.team__slider');

const activeSliderTeamSection = function(){

        if (window.innerWidth > 768){
            if(teamSlider.classList.contains('swiper-container-initialized')){
                sliderTeam.destroy();
            }
            sliderTeam = new Swiper('.team__slider', {
                effect: "coverflow",
                grabCursor: true,
                centeredSlides: true,
                slidesPerView: 3,
                coverflowEffect: {
                  rotate: 50,
                  stretch: 0,
                  depth: 100,
                  modifier: 1,
                  slideShadows: true,
                },
                loop: true,
            });
        }

        if(window.innerWidth < 768){
            if(teamSlider.classList.contains('swiper-container-initialized')){
                sliderTeam.destroy();
            }
            sliderTeam = new Swiper('.team__slider', {
                grabCursor: true,
                spaceBetween: 10,
                slidesPerView: 'auto',
                direction: 'horizontal',
                loop: true,
            });
        }
}

activeSliderTeamSection();

window.addEventListener('resize', function(event){
    activeSliderTeamSection();
    mobileMenu();
})

const upButton = () => {

    const body = document.querySelector('body'),
          div = document.createElement('div');
    
    div.className = 'up__arrow';
    
    body.appendChild(div);
    
    div.addEventListener('click', () =>{
        scrollTo(header);
    });
    
    window.addEventListener('scroll', function(){
        let heightPage = +this.pageYOffset;
        if(heightPage >= 450){
            div.style.display = 'block';
        } else {
            div.style.display = 'none';
        }
    });
}


//Прокрутка по разделам лендинга
const linkScroll = document.querySelectorAll('.link__scroll'),
      header = document.querySelector('.header'),
      services = document.querySelector('.services'),
      team = document.querySelector('.team'),
      contact = document.querySelector('.contact');

const pageScroll = [
        {name: header},
        {name: services},
        {name: team},
        {name: contact},
];

for(let i = 0;  i < linkScroll.length; i++){
    linkScroll[i].addEventListener('click', (target)=>{
        target.preventDefault();
        const getPages = pageScroll.map(elem => elem.name);
        let classPages = getPages[i];
        if (classPages){
            scrollTo(classPages);
        }
    })
}

function scrollTo(elem){
    window.scroll({
        left: 0,
        top: elem.offsetTop,
        bottom: elem.offsetBottom,
        behavior: 'smooth'
    })
}

upButton();